"use strict";

function Block(){
	return {
		position: {x:0, y: 0, z:0},
		screenPosition: {x: 0, y: 0},
		visible: true,
		img: null,
		
		setPosition: function(x,y,z, size){
			this.position = {x: x, y: y, z: z};
			this.screenPosition.x = x * (size / 2 ) + z * (size / 2);
			this.screenPosition.y = x * (size / 4) + z * (size / 4) - y * (size / 2 );
		}
	};
}