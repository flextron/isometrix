"use strict";

function Game(){
	return {
		canvas: {},
		ctx: {},
		fpsLimit: 10,
		drawPool: [],
		cellSize: 32, //Cell size in pixels
		mapObjects:[], //Objects in matrix x,y,z
		mapSize:{x: 5, y: 20, z: 5}, // Map size
		mapOffset: {x: 0, y: 0}, //Offset of drawn objects (For drawing sprites in center of screen)
		sprites: {},
		loader: {
			spritesLeft: 0
		},
		
		Init: function(canvas_id, mapSize, callbackReady){ //Initialization (Canvas ID, MapSize: {x:0 , y:0, z:0})
			this.mapSize = mapSize;
			this.canvas = document.getElementById(canvas_id);
			this.ctx = this.canvas.getContext("2d");
            this.canvas.width = window.innerWidth;
            this.canvas.height = window.innerHeight;
			this.mapOffset.x = this.canvas.width / 2;
			this.mapOffset.y = this.canvas.height / 2;
			this.mapOffset.x -= this.mapSize.x * this.cellSize / 2;
			this.mapOffset.y += this.mapSize.y * this.cellSize / 4;

			this.SpriteLoader('stone','src/images/block_stone.png', callbackReady);
			this.SpriteLoader('dirt','src/images/block_dirt.png', callbackReady);
			this.SpriteLoader('grass','src/images/block_grass.png', callbackReady);
		},
		
		Draw: function(){ //Just clearing last frame and draw background color
			this.ctx.fillStyle = "#91b7ce";
			this.ctx.fillRect(0,0,2000,2000);
		},

		DrawFps: function(fps){
			this.ctx.fillStyle = '#000';
			this.ctx.font="20px Arial";
			this.ctx.fillText("FPS: " + fps,10,20);
		},
		
		DrawImage: function(src, x, y){ //Sprite drawing
			this.ctx.drawImage(src, x, y, src.width, src.height);
		},
		
		DrawMap: function(){ //Sraw all objects inside mapObjects matrix
			for(var y = 0; y < this.mapSize.y; y++){ // First of all we draw lower cells (lowest y value) "the ground"
				for(var z = 0; z < this.mapSize.z; z++){ // Now the Z value, lowest is 0 (more further from camera)
					for(var x = this.mapSize.x - 1; x >= 0; x--){ // And now X. X sorted DESC because right object has to be overrided by object from left side
						if(this.mapObjects[x][y][z] && this.mapObjects[x][y][z].visible == true){//If cell is filled with Block and this block is (visible = true)
							this.DrawImage(
								this.mapObjects[x][y][z].img,
								x * (this.cellSize / 2 ) + z * (this.cellSize / 2) + this.mapOffset.x,
								x * (-this.cellSize / 4) + z * (this.cellSize / 4) + this.mapOffset.y - y * (this.cellSize / 2 )
							);
						}
					}
				}
			}
			
		},
		
		GenerateMap: function(){ // Map generator ( Filling cells with blocks )
			this.mapObjects = [];
			
			for(var x = 0; x < this.mapSize.x; x++){
				this.mapObjects[x] = [];
				
				for(var y = 0; y < this.mapSize.y; y++){
					this.mapObjects[x][y] = [];
					
					for(var z = 0; z < this.mapSize.z; z++){
						if(Math.random() < 0.1 + y / this.mapSize.y ){ //Put block into cell with 90% chance
							//console.log(this.sprites);
							this.mapObjects[x][y][z] = new Block();
							this.mapObjects[x][y][z].setPosition( x, y, z, this.cellSize );
							this.mapObjects[x][y][z].img = Math.random() > (y / this.mapSize.y) ? this.sprites['stone'][y] : this.sprites['dirt'][y];// Set block image black or green (50/50)
							this.mapObjects[x][y][z].visible = false;
							if(y == this.mapSize.y -1)
								this.mapObjects[x][y][z].img = this.sprites['grass'][y];// Set block image black or green (50/50)
						}
						
					}
				}
			}
			
		},

		SpriteLoader: function(spriteName, url, callbackReady){
			var _this = this;
			_this.sprites[spriteName] = [];
			_this.loader.spritesLeft++;
			var img = new Image();
			img.src = url;
			
			img.onload = function(){
				for(var y = 0; y < _this.mapSize.y; y++){
					var canvasTmp = document.createElement('canvas');
					var ctxTmp = canvasTmp.getContext("2d");
					var filter = 0.8 + (y / _this.mapSize.y - 0.5) / 1.5;
					canvasTmp.width = _this.cellSize;
					canvasTmp.height = _this.cellSize;
					ctxTmp.clearRect(0, 0, canvasTmp.width, canvasTmp.height);
					ctxTmp.filter = 'brightness('+ filter +')';
					ctxTmp.drawImage(img,0,0);
					_this.sprites[spriteName].push( canvasTmp );
				}
					
				_this.loader.spritesLeft--;


				if(_this.loader.spritesLeft <= 0){
					if(callbackReady){
						_this.GenerateMap();
						callbackReady();

					}
				}
			};
		}
	};
}