"use strict";
var url = new URL(window.location.href);

var mapSizeArray = url.searchParams.get('size') ? url.searchParams.get('size').split(',') : [1,1,1];
var mapSize = {
	x: mapSizeArray[0] || 1, 
	y: mapSizeArray[1] || 1, 
	z: mapSizeArray[2] || 1
};

var game = new Game(); //Game object
var pong = 1; //Pong animation (show layers and hide them)
var layer = 0; //Current layer for animation (starting from -1 because with first loop it will be increased by pong value (1) )
var radius = -1;
var cellCenter = {x: 0, z: 0};
var deltaTime = 0;
var deltaTimeTmp = Date.now();
var timeLeft = Date.now();
var loopsCount = 0;
var fps = 0;

game.Init("gameCanvas" , mapSize, MainLoop );//Game initialization + size of game map (game world)
game.fpsLimit = 90;
cellCenter.x = Math.floor(mapSize.x / 2);
cellCenter.z = Math.floor(mapSize.z / 2);

function MainLoop(){
	game.Draw(); //Clearing screen and colorizing background with blue sky color
	game.DrawMap(); // Draw all cells that are visible (Block.visible == true)
	
	if( (pong > 0 && layer >= game.mapSize.y) || (pong < 0 && layer <= 0) ){ //Change pong to 1/-1 if it greater mapSize.y or less than 0
		pong *= -1;
	}
	//layer += pong;
	radius++;
	
	//var y = layer;
	// for(var x = 0; x < game.mapSize.x; x++){
	// 	for(var z = 0; z < game.mapSize.z; z++){
	// 		if(layer < game.mapSize.y && game.mapObjects[x][y][z]){
	// 			if(pong > 0)
	// 				game.mapObjects[x][y][z].visible = true; //Set block visible if we increasing layer value
	// 			else
	// 				game.mapObjects[x][y][z].visible = false; //Set block hidden if we decreasing layer value
	// 		}
	// 	}
	// }

	var cellList = GetRadius(cellCenter, radius, game.mapSize);
	cellList.map(function(cell){
		if( layer < game.mapSize.y && game.mapObjects[cell.x][layer][cell.z]){
			game.mapObjects[cell.x][layer][cell.z].visible = true;
		}
	});

	if(cellList.length <= 0 && layer < game.mapSize.y){
		layer++;
		radius = -1;
		if(layer >= game.mapSize.y)
			return;
	}

	deltaTime = Date.now() - deltaTimeTmp;
	
	deltaTimeTmp = Date.now();
	loopsCount++;

	if(Date.now() - timeLeft >= 1000){
		fps = loopsCount;
		loopsCount = 0;
		timeLeft = Date.now();
	}
	game.DrawFps(fps);

	setTimeout(function(){//Animation of layers(y) show/hide
		MainLoop();
	},1000 / game.fpsLimit);
}

function GetRadius (center, radius, mapSize){
	var list = [];
	for(var x = -radius; x <= radius; x++){
		var newCell = {
			x: x + center.x,
			z: radius - Math.abs(x) + center.z
		};

		if(newCell.x >= 0 && newCell.x < mapSize.x && newCell.z >= 0 && newCell.z < mapSize.z )
			list.push(newCell);
	}
	for(var x = -radius + 1; x <= radius - 1; x++){
		var newCell = {
			x: x + center.x,
			z: Math.abs(x) - radius + center.z
		};

		if(newCell.x >= 0 && newCell.x < mapSize.x && newCell.z >= 0 && newCell.z < mapSize.z )
			list.push(newCell);
	}

	return list;
}